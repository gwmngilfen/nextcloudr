
<!-- README.md is generated from README.Rmd. Please edit the Rmd file -->

# NextcloudR

R interface to the Nextcloud WebDAV API

NextcloudR gives you similar functionality to the [GoogleDrive
package](https://googledrive.tidyverse.org), but for your *own* cloud.

It also supports the new(ish) [{pins}
package](https://rstudio.github.io/pins) from RStudio, which makes it
very easy to “pin” data to your Nextcloud instance. See the Pins section
below.

**Authors:** [Greg Sutcliffe](https://emeraldreverie.org) **License:**
[MIT](https://opensource.org/licenses/MIT)

Much thanks to JennyBC for the inspiration, and the [meetupr
package](https://github.com/rladies/meetupr) for code examples\!

## Installation

To install the development version from GitLab:

``` r
# install.packages("devtools")
devtools::install_gitlab("gwmngilfen/nextcloudr")
```

There may be a CRAN version eventually, but this is still very much in
development for now.

## Usage

To use this package, you will first need an “app password” from
Nextcloud. To get one, head to your Nextcloud instance, go to Settings
\> Security \> Devices & Sessions \> Create New App Password.

Once you have your app password, save it (and your username and server)
as an environment variable for the current session by running the
following:

``` r
Sys.setenv(NEXTCLOUD_USER   = "GregSutcliffe",
           NEXTCLOUD_PASS   = "12345-ABCDE-abcdf-zYXwV-98765",
           NEXTCLOUD_SERVER = "https://mynextcloud.example.org")
```

You could also save this to your .RProfile or similar

If you don’t want to save it here, you can input it in each function
using the `.user`, `.pass`, and `.server` parameters (just be sure not
to send any documents with your key to GitHub).

## Function naming convention

The package use consistent prefixes:

    nc_*   = all functions in the package.

Here’s an example of listing files:

    > nc_ls()
    # A tibble: 114 x 5
       file_name                                author     filesize perms updated         
       <chr>                                    <chr>      <chr>    <chr> <chr>           
     1 /                                        Greg Sutc… ""       RGDN… Thu, 04 Apr 201…
     2 /APKs/                                   Greg Sutc… ""       RGDN… Sat, 10 Feb 201…
     3 /APKs/com.keramidas.TitaniumBackup_8.0.… Greg Sutc… 8043260  RGDN… Mon, 05 Jun 201…
     4 /APKs/com.keramidas.TitaniumBackup_8.0.… Greg Sutc… 8044926  RGDN… Wed, 05 Jul 201…
     ...

Filesize of an empty string usually denotes a directoy.

You can look in a subdir:

    > nc_ls('/Delve/Manuals')
    # A tibble: 14 x 5
      file_name                            author      filesize perms  updated            
      <chr>                                <chr>       <chr>    <chr>  <chr>              
     1 /Delve/Manuals/                      Greg Sutcl… ""       RGDNV… Sun, 07 May 2017 0…
     2 /Delve/Manuals/Argos%20TS-EE8%20Tim… Greg Sutcl… 320491   RGDNVW Fri, 14 Oct 2016 0…
     3 /Delve/Manuals/Ford-C-MAX-manual.pdf Greg Sutcl… 6608449  RGDNVW Sun, 07 May 2017 0…
     4 /Delve/Manuals/Sureflap%20Microchip… Greg Sutcl… 539182   RGDNVW Sun, 01 Jan 2017 1…
     ...

Or search via a regex:

    > nc_ls(regex = '.apk$')
    # A tibble: 13 x 5
       file_name                                 author    filesize perms updated         
       <chr>                                     <chr>     <chr>    <chr> <chr>           
     1 /APKs/com.keramidas.TitaniumBackup_8.0.1… Greg Sut… 8043260  RGDN… Mon, 05 Jun 201…
     2 /APKs/com.keramidas.TitaniumBackup_8.0.2… Greg Sut… 8044926  RGDN… Wed, 05 Jul 201…
     3 /APKs/com.keramidas.TitaniumBackup_8.1.0… Greg Sut… 8048194  RGDN… Sat, 10 Feb 201…
     4 /APKs/com.plexapp.android_6.1.1.656-6011… Greg Sut… 32480118 RGDN… Wed, 05 Jul 201…
      ...

You can upload files:

    write_csv(iris,'iris.csv')
    nc_upload('/tmp/iris.csv')

and optionally specify a path:

    write_csv(iris,'iris.csv')
    nc_upload('/tmp/iris.csv','/foo/iris.csv')

You don’t have to upload only text, e.g. a JPEG:

    jpeg('/tmp/iris.jpg')
    ggplot(iris,
           aes(x = Petal.Length,
               y = Petal.Width,
               color = Species)) +
      geom_point()
    dev.off()
    nc_upload('/tmp/iris.jpg')

Downloading files works too, of course, defaulting to the current
directory, but that can be overriden:

    nc_download('/iris.csv') # goes to ./iris.csv
    nc_download('/iris.csv','/tmp/iris_local.csv')

And we can clean up:

    nc_delete('/iris.csv')

# Using NextcloudR with Pins

As of 0.2.0 NextcloudR supports Pins. First, set up your enviroment with
the authentication variables as above in “Usage”. Then you can do
something like:

    library(nextcloudr)
    library(pins)
    
    Sys.setenv(NEXTCLOUD_USER   = "?",
               NEXTCLOUD_PASS   = "?",
               NEXTCLOUD_SERVER = "https://?")
    
    board_register('nextcloud')
    
    pin(mtcars,'mtcars',board = 'nextcloud')
    
    pin_find(board = 'nextcloud')
    
    pin_get('mtcars','nextcloud')
    
    pin_remove('mtcars',board='nextcloud')

Refer to the [Pins documentation](https://rstudio.github.io/pins/) for
more general usage of Pins.

## How can you contribute?

Feel free to pick anything off the TODO list below, or from the issues
list, or any other functionality you think is missing\!

### TODO:

  - better error messages
  - things in the Pins code
  - Fix package checks
  - add tests
  - other commands
      - mkdir
      - cp
      - mv
  - Nextcloud-y things like
      - share a file with a username
      - star a file
      - publish a public link to a file
      - Proper browser-based OAuth token workflow
