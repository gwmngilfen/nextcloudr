#' List files in a Nextcloud directory
#'
#' List the files found in the specified Nextcloud directory. This lists
#' everything, even though the package currently only supports spreadsheets.
#' Environment variables are used to hold the authentication, see the examples.
#'
#' @param dir character; a directory to list
#' @param regex character; a regular expression to filter the results by
#' @param ... optional arguments to be passed to \code{\link{grep}} when
#'   matching \code{regex} to sheet titles
#' @param verbose logical; verbose output
#' @param .user character; override the user to login as
#' @param .password character; override the password for .user
#' @param .server character; override the location of the Nextcloud server
#'
#' @return a \code{\link[dplyr]{tbl_df}} with one row per file
#'
#' @examples
#' \dontrun{
#' # Authentication
#' Sys.setenv(NEXTCLOUD_USER   = "GregSutcliffe",
#'            NEXTCLOUD_PASS   = "12345-ABCDE-abcdf-zYXwV-98765",
#'            NEXTCLOUD_SERVER = "https://mynextcloud.example.org")
#'
#' nc_ls()
#' nc_ls('my_dir')        # a subdirectory
#' nc_ls(regex = '.csv$') # a regular expression search
#' }
#'
#' @export
nc_ls <- function(dir = '/', regex = NULL, ...,
                  .user = NULL, .password = NULL, .server = NULL,
                  verbose = TRUE) {

  creds <- .validate_credentials(list(user = .user,
                                      pass = .password,
                                      server = .server))

  prop_body='<?xml version="1.0"?>
<d:propfind  xmlns:d="DAV:" xmlns:oc="http://owncloud.org/ns" xmlns:nc="http://nextcloud.org/ns">
  <d:prop>
        <d:getlastmodified />
        <d:getetag />
        <d:getcontenttype />
        <d:resourcetype />
        <oc:fileid />
        <oc:permissions />
        <oc:size />
        <d:getcontentlength />
        <nc:has-preview />
        <oc:favorite />
        <oc:comments-unread />
        <oc:owner-display-name />
        <oc:share-types />
  </d:prop>
</d:propfind>'
  url = paste0(creds$server,"/remote.php/dav/files/",creds$user,"/",dir)

  req <- httr::VERB("PROPFIND", url,
              httr::authenticate(creds$user,creds$pass),
              httr::add_headers(depth = 2),
              body = prop_body) %>%
    httr::stop_for_status('PROPFIND error in listing DAV folder')
  #rc <- content_as_xml_UTF8(req) - helper in Googlesheets Package
  rc <- httr::content(req)

  ns <- xml2::xml_ns_rename(xml2::xml_ns(rc), d = "dav")

  ret <- dplyr::data_frame_(list(
    file_name =
      ~ rc %>% xml2::xml_find_all(".//dav:href", ns) %>%
      xml2::xml_text() %>%
      sub(pattern = paste0('/remote.php/dav/files/',creds$user), replacement = ''),
    author =
      ~ rc %>% xml2::xml_find_all(".//dav:prop/oc:owner-display-name", ns) %>%
      xml2::xml_text(),
    filesize =
      ~ rc %>% xml2::xml_find_all(".//dav:prop/dav:getcontentlength", ns) %>%
      xml2::xml_text(),
    perms =
      ~ rc %>% xml2::xml_find_all(".//dav:prop/oc:permissions", ns) %>%
      xml2::xml_text(),
    updated =
      ~ rc %>% xml2::xml_find_all(".//dav:prop/dav:getlastmodified", ns) %>%
      xml2::xml_text()
  ))

  if(is.null(regex)) {
    return(ret)
  } else {
    stopifnot(inherits(regex, "character"))
  }

  if(length(regex) > 1) {
    regex <- regex %>% paste(collapse = "|")
  }
  keep_me <- grep(regex, ret$file_name, ...)

  if(length(keep_me) == 0L) {
    if(verbose) {
      message("No matching sheets found.")
    }
    invisible(NULL)
  } else {
    ret[keep_me, ]
  }
}
